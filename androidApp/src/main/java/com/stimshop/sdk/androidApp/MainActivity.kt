package com.stimshop.sdk.androidApp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.switchmaterial.SwitchMaterial
import com.stimshop.sdk.*
import com.stimshop.sdk.common.configuration.sdk.DetectionMode
import com.stimshop.sdk.common.configuration.sdk.ResourcesConfiguration
import com.stimshop.sdk.utils.StimshopBus
import com.stimshop.sdk.utils.withContext
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rv: RecyclerView = findViewById(R.id.list)
        val adapter = RecyclerAdapter()
        rv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv.adapter = adapter

        StimShop.Builder()
            .withContext(this)
            .debugEnabled(true)
            .build()

        val eventList = mutableListOf<String>()
        val pattern = "HH:mm:ss.SSS"
        val simpleDateFormat = SimpleDateFormat(pattern)

        StimshopBus.onStimshopEvent(this) {
            val formattedDate = simpleDateFormat.format(Date())

            when (it) {
                is StimShopReady -> {
                    eventList.add("$formattedDate - SDK isReady = ${it.isReady}")
                    StimShop.get().startDetection()
                }
                is NoAuthorizedSignals -> {
                    eventList.add("$formattedDate - ${it.message}")
                }
                is MissingPermissions -> {
                    eventList.add("$formattedDate - SDK Missing permissions - ${it.permissions}")
                    requestPermissions(it.permissions.toTypedArray(), 100)
                }
                is StimShopDetectChirp -> {
                    eventList.add("$formattedDate - Chirp detecté sur le channel ${it.channelId}")
                }
                is StimShopDetectCode -> {
                    eventList.add("$formattedDate - Code décodé ${it.code}")
                }
                is StimShopDetectNotification -> {
                    eventList.add("$formattedDate - Notification ${it.item.id} - ${it.notification.displayName}")
                }
                is WiUSChecksumError -> {
                    eventList.add("$formattedDate - Checksum error : ${it.message}")
                }
                is DetectionStarted, DetectionStopped -> {

                }
                else -> {
                    eventList.add("$formattedDate - error")
                }
            }
            adapter.items = eventList
            adapter.notifyDataSetChanged()
        }

        val switch: SwitchMaterial = findViewById(R.id.switch_mode)
        switch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                ResourcesConfiguration.detectionMode = DetectionMode.WIUS
            } else {
                ResourcesConfiguration.detectionMode = DetectionMode.UCHECKIN
            }
            StimShop.get().restart()
            eventList.clear()
            adapter.items = eventList
            adapter.notifyDataSetChanged()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        StimShop.get().restart()
    }
}


class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    var items: List<String> = listOf()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val event: TextView = itemView.findViewById(R.id.event)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.event_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = items[position]
        holder.event.text = event
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

