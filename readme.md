

# stimshop-sdk

## Description

StimshopSDK implements Stimshop's signal detection protocol.

This SDK scans audio frequencies in the environment through the microphone to trigger events.

This SDK is developped in `Kotlin KMM`.

## Setup

Copy the two `.aar` files (jnifft-release.aar, stimshop-sdk-commons-x-x-x.aar) in the ` app/libs ` folder of your own project 


- Add the Jitpack repository to your root build.gradle:

```gradle
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
```

- Add the apollo classpath to your root build.gradle

```gradle
buildscript {
    dependencies {
        ...
        classpath("com.apollographql.apollo3:apollo-gradle-plugin:3.7.4")
    }
}
```


- Add the following line to your application module's `build.gradle` file, in the dependencies section:

````gradle
// This is required to locate the SDK's AAR file specified in the dependencies block
repositories {
    flatDir {
        dirs 'lib'
    }
}

dependencies {
     implementation fileTree(include: ['*.jar', '*.aar'], dir: 'libs')
    // Ktor
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")

    // Datastore
    implementation("com.icecreamhappens.secureprefs:common:0.4.0")

    // GraphQL
    implementation("com.apollographql.apollo3:apollo-runtime:3.7.4")
    implementation("com.apollographql.apollo3:apollo-adapters:3.7.4")

    //Jnifft
    implementation "org.apache.commons:commons-math3:3.6.1"
}
````

Now you can synchronize the project with the gradle files (either from the toolbar button or from the
menu `Tools > Android > Sync project with Gradle files`.

> **TODO** Using a Maven repository would allow the user to only specify the SDK dependency. All other
> dependencies would be pull automatically.

Create `stimshop.xml` file in `res/values` :

````xml
<?xml version="1.0" encoding="utf-8"?>  
<resources>  
 <!-- YOUR CREDENTIALS -->  
 <string name="stimshop_api_key" translatable="false">your_api_key</string>  
 <string name="stimshop_api_token" translatable="false">your_api_token</string>  
  
  <!-- CHANNELS TO ENABLE -->  
  <integer-array name="stimshop_channels">  
  <item>1</item>  
  <item>2</item>  
  <!-- etc. -->  
  </integer-array>  
  
  <!-- DETECTION MODE TO ENABLE  
     0 -> Uchecin 
     1 -> WiUS 
  -->  
  <integer name="stimshop_detection_mode">0</integer>  
</resources>
````

You can configure the channels you want to use:

- 1: from 17 to 18KHz
- 2: from 18 to 19KHz
- 3: from 19 to 20KHz
- 4: from 20 to 21KHz

and the initial detection mode:

- 0: ucheck-in mode
- 1: wi-us mode



## Usage


### Initialization

For example, inside an `Activity`

````kotlin  
override fun onCreate(savedInstanceState: Bundle?) {  
    ... 
    StimShop.Builder()  
        .withContext(this)  
        .debugEnabled(true)  
        .extraMessage("Extra message")  
        .build()
}
````  
Or in java:

````java  
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
    ... 
    StimShop.Builder builder = ExtensionsKt.withContext(new StimShop.Builder()
                .debugEnabled(true).extraMessage("Extra message"), this);
    StimShop stimShop = builder.build();
    ... 
}

````  



### Add permissions

```xml
    <!-- ALLOW RECORDING AUDIO TO DETECT SIGNALS -->
    <uses-permission android:name="android.permission.RECORD_AUDIO" />

    <!-- INTERNET ACCESS REQUIRED TO CHECK API KEY AND GET RELATED SIGNALS -->
    <uses-permission android:name="android.permission.INTERNET" />

    <!-- REQUIRED TO CHECK NETWORK AVAILABILITY -->
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

    <!-- OPTIONAL, ONLY REQUIRED IF YOU WANT TO USER STIMSHOP API -->
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />

    <!-- Notice the use of the microphone to the user (required for earlier Android APIs)  -->
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE"/>

````

### Observe stimshop bus events

To receive bus events, add your activity or fragment as an observer.  
When an event is to be sent, the callback code will be executed with a `StimShopEvent` as input parameter.

````kotlin
StimshopBus.onStimshopEvent(this) { stimshopEvent ->
    when (stimshopEvent) {
        is StimShopReady -> {
            //Called when the detectors have been instantiated and checked
            //SDK is ready to start the scan of the environment
        }
        is NoAuthorizedSignals -> {
            //Signal unauthorized detected
        }
        is MissingPermissions -> {
            //Failed to start the detection caused by missing permissions  
            //Lists the missing permissions with it.permissions, request for it, and call StimShop.get().restart()
        }
        is StimShopDetectChirp -> {
            //Chirp dected on a channel
        }
        is StimShopDetectCode -> {
            //Code detected on a channel
        }
        is StimShopDetectNotification -> {
            //Code has been linked to a notification  
            //Use this notification data
        }
        is WiUSChecksumError -> {
            //Checksum Error related to WiUS
        }
        is DetectionStarted -> {
            //Detection started
        }
        is DetectionStopped -> {
            //Detection stopped
        }
        is StimShopError -> {
            //Error occured
        }
    }
}
````

Or in java:

````java  
    StimshopBus.INSTANCE.onStimshopEvent(this, new Function1<>() {
            @Override
            public Unit invoke(StimShopEvent stimShopEvent) {
                String formattedDate = simpleDateFormat.format(new Date());
                if (stimShopEvent instanceof StimShopReady){
                  //Called when the detectors have been instantiated and checked
                  //SDK is ready to start the scan of the environment
                }else if (stimShopEvent instanceof NoAuthorizedSignals) {
                 //Signal unauthorized detected
                }
                else if (stimShopEvent instanceof  MissingPermissions) {
                //Failed to start the detection caused by missing permissions  
                //Lists the missing permissions with it.permissions, request for it, and call StimShop.get().restart()
                }else if (stimShopEvent instanceof StimShopDetectChirp) {
                //Chirp dected on a channel
                }
                else if (stimShopEvent instanceof StimShopDetectCode) {
               //Code detected on a channel
                }
                else if (stimShopEvent instanceof StimShopDetectNotification) {
                    //Code has been linked to a notification  
                    //Use this notification data
                }
                else if (stimShopEvent instanceof WiUSChecksumError) {
                     //Checksum Error related to WiUS
                }
                else if (stimShopEvent instanceof DetectionStarted) {
                     //Detection started
                }
                else if (stimShopEvent instanceof DetectionStopped) {
                    //Detection stopped
                }
                else if (stimShopEvent instanceof StimShopError){
                     //Error occured
                }
                return null;
            }
        });

````  

#### StimShopEvents


````kotlin
StimShopReady(val isReady: Boolean)
```` 

````kotlin
NoAuthorizedSignals(val message: String)
````

````kotlin
MissingPermissions(val permissions: List<String>)
````

````kotlin
StimShopDetectChirp(val channelId: Int)
````

````kotlin
StimShopDetectCode(val code: String, val channelId: Int)
````

````kotlin
StimShopDetectNotification(val notification: StimNotification, val item: StimNotificationItem)
````

````kotlin
WiUSChecksumError(val message: String)
````

````kotlin
StimShopError(val code: Int, val errorMessage: String?)
````

````kotlin
DetectionStarted()
````

````kotlin
DetectionStopped()
````

### Notification related classes (KMM)

````kotlin
class StimNotification {
    lateinit var name: String
        internal set
    lateinit var displayName: String
        internal set
    lateinit var type: StimNotificationType
        internal set
    lateinit var startDate: String
        internal set
    lateinit var endDate: String
        internal set
    lateinit var signal: String
        internal set
    var iconUrl: String? = null
        internal set
    var eventId: String? = null
        internal set
    lateinit var items: List<StimNotificationItem>
        internal set
    var delayBetweenDisplay: Int? = null
        internal set
    }
```` 

````kotlin
class StimNotificationItem {
    lateinit var id: String
        internal set
    lateinit var displayType: String
        internal set
    var imageUrl: String? = null
        internal set
    var imageLink: String? = null
        internal set
    var htmlContent: String? = null
        internal set
    var videoLink: String? = null
        internal set
    var numberOfDisplay by Delegates.notNull<Int>()
        internal set
    var delayBetweenDisplay by Delegates.notNull<Int>()
        internal set
    var probability: Int? = null
        internal set
    var quota: Int? = null
        internal set
    var defaultNotification: Boolean = false
        internal set
    var iconUrl: String? = null
        internal set
}
````

````kotlin
enum class StimNotificationType(val value: String) {
    SIMPLE("simple"), RANDOM("random-draw"), INSTANT_WIN("instant-win"), TREASURE_HUNT("treasure-hunt");
}
````


### Start detection

When `StimShopReady`, you can start the detection and receive informations of the detection as the [previous section](#observe-stimshop-bus-events) mentioned. 

````kotlin  
StimShop.get().startDetection()
````  

### Stop the detection when it is no longer needed

You must stop the detection when it is no longer needed. You can stop it on the onDestroy() method to avoid any problem when destroying your activity.

````kotlin  
override fun onDestroy() {
    StimShop.get().stopDetection()  
    super.onDestroy()  
}
````  

### Check if SDK is currently detecting


````kotlin  
StimShop.get().isDetecting() : Boolean
```` 

### Restart detection

Used typically after adding missing permissions.

````kotlin  
StimShop.get().restart()
```` 